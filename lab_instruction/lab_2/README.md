# Lab 2: Pengenalan _Django Framework_

CSGE602022 - Perancangan & Pemrograman Web (Perancangan & Pemrograman Web) @
Fakultas Ilmu Komputer Universitas Indonesia, Semester Genap 2017/2018

* * *

## Tujuan Pembelajaran

Setelah menyelesaikan tutorial ini, mahasiswa diharapkan untuk mengerti :

- Mengerti Struktur _Django Project_
- Mengerti bagaimana alur _Django_ menampilkan sebuah _webpage_
- Mengerti konfigurasi yang ada pada _settings.py_

## Kebutuhan praktikum
- Text Editor (apapun merk-nya, yang penting bisa menulis & edit text)
- Python 3.6 terinstall di komputer Anda
- Akses internet
- Git client
- Repository GitLab untuk tutorial
- Direktori pekerjaan tutorial

## Instruksi Lab 2: Pengenalan _Django Framework_

1. Buka terminal (command prompt untuk windows) dan masuk ke direktori sumber-kode-instruksi (direktori kalian dari step 1 pada lab 1).

2. Mendownload kode contoh dari Server kode PPW-2017-Genap bertujuan untuk meng-update direktori sumber-kode-instruksi anda sesuai dengan update pada repository PPW-2017-Genap pada gitlab. Hal ini dilakukan dengan cara pull dengan git
> Melakukan pull dari git remote PPW-2017-Genap dengan cara git pull origin master

3. Buka direktori kode-saya (direktori kalian dari step 2 pada lab 1) menggunakan terminal. Buatlah sebuah `apps` lab_2 dengan `Django framework`
> Gunakan perintah : python manage.py startapp lab_2

4. Pastikan file di dalam lab_2 berisi: (jika ada direktori yang kurang, buatlah direktori tersebut)
>       - __init__.py
>       - admin.py
>       - apps.py
>       - models.py
>       - tests.py
>       - views.py
>       - migrations/
>       - templates/

5. Editlah file `tests.py` pada `lab_2` agar berisi:
    ```python
    from django.test import TestCase
    from django.test import Client
    from django.urls import resolve
    from .views import index, landing_page_content, mhs_name
    from django.http import HttpRequest
    from unittest import skip

    # Create your tests here.

    class Lab2UnitTest(TestCase):

        def test_lab_2_url_is_exist(self):
            response = Client().get('/lab-2/')
            self.assertEqual(response.status_code,200)

        def test_lab2_using_index_func(self):
            found = resolve('/lab-2/')
            self.assertEqual(found.func, index)

        def test_landing_page_content_is_written(self):
            #Content cannot be null
            self.assertIsNotNone(landing_page_content)

            #Content is filled with 30 characters at least
            self.assertTrue(len(landing_page_content) >= 30)

        def test_landing_page_is_completed(self):
            request = HttpRequest()
            response = index(request)
            html_response = response.content.decode('utf8')
            self.assertIn('Hello, this is '+ mhs_name +'.', html_response)
            self.assertIn(landing_page_content, html_response)
    ```

6. Lakukan `git add` dan `git commit`. Setelah itu cobalah `python manage.py test`
> Jika hal ini dilakukan akan terjadi Fail. Mengapa? (lakukan analisa)

7. Editlah file `views.py` pada `lab_2` dengan
    ```python
    from django.shortcuts import render
    from lab_1.views import mhs_name, birth_date

    #TODO Implement
    #Create a content paragraph for your landing page:
    landing_page_content = ''

    def index(request):
        response = {'name': mhs_name, 'content': landing_page_content}
        return render(request, 'index_lab2.html', response)
    ```

8. Buatlah file `urls.py` pada `lab_2`

9. Editlah file `urls.py` pada `lab_2` dengan
    ```python
    from django.conf.urls import url
    from .views import index
    #url for app
    urlpatterns = [
        url(r'^$', index, name='index'),
    ]
    ```

10. Bukalah direktori `lab_2/templates/`. Lalu buatlah file `index_lab2.html` dan isilah file tersebut dengan kode sebagai berikut:
    ```python
    {% extends "base.html" %}
    {% block content %}
    <style>
        .center_screen {
            position: absolute;
            width: 500px;
            height: 50px;
            top: 50%;
            left: 50%;
            margin-left: -250px; /* margin is -0.5 * dimension */
            margin-top: -25px;
        }
        .wujud{
            font-size: 30px;
            font-family: "Comic Sans MS";
            color: black;
        }
    </style>
    <div class="center_screen">
        <p class="wujud status" style="text-align: center" id="test">Hello, this is {{ name }}. {{ content }}.</p>
    </div>
    {% endblock %}
    ```

11. Editlah `settings.py` pada folder `praktikum` dan cari `INSTALLED_APPS`, akan terlihat seperti ini
    ```python
    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'lab_1',
        'lab_2', #tambahkan lab_2 pada setting
    ]
    ```

12. Editlah `urls.py` pada folder `praktikum` seperti ini
     ```python
     from django.conf.urls import url, include
     from django.contrib import admin
     import lab_1.urls as lab_1
     import lab_2.urls as lab_2
     from lab_1.views import index as index_lab1
     from lab_2.views import index as index_baru

     urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^lab-1/', include(lab_1,namespace='lab-1')),
        url(r'^lab-2/', include(lab_2,namespace='lab-2')),
        url(r'^$', index_baru, name='index')
     ]
     ```

13. Cobalah `python manage.py test`, lakukan analisa dan ubah kode anda jika masih ada error

14. Jalankan program dengan secara lokal
> python manage.py runserver 8000

15. Akses ke web server lokal Anda dengan browser, dengan menuliskan alamat `http://localhost:8000` pada browser Anda.
> Cobalah klik link pada header yang ada pada web. Kemanakah web diarahkan? Kenapa halaman tersebut bisa terbuka?

16. Selamat kalian berhasil membuat landing page dengan bantuan fitur dari `Django Framework`
